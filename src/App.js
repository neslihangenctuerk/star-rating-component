import React from 'react'
import './App.css'
import Rating from './Rating'


class App extends React.Component {

  state = {
    currentRating: 0,
    maxRating: 5,
    ratingsList: [
      {
        id: 0,
        selected: false
      },
      {
        id: 1,
        selected: false 
      },
      {
        id: 2,
        selected: false
      },
      {
        id: 3,
        selected: false
      },
      {
        id: 4,
        selected: false
      }
    ]
   
  }

  handleClick = (ratingVal) => {
    
    this.setState((state) => {
      let rVal = parseInt(ratingVal, 10)
      let updatedRatingsList

      if(state.currentRating < rVal){
          updatedRatingsList = state.ratingsList.map(rating => {
          if(rating.id <= ratingVal) {
            rating.selected = true
          }
          return rating
        })
  
          return {
            ratingsList: updatedRatingsList,
            currentRating: rVal + 1
          }
      } else {
      
       console.log("Rating seems to be lower than current Heighest - current val: ", rVal) 
    


       if(state.currentRating > rVal) {
          updatedRatingsList = state.ratingsList.map(rating => {
            if(rating.id > rVal) {
              rating.selected = false
            }
            return rating
          })

          return {
            ratingsList: updatedRatingsList,
            currentRating: rVal + 1
          }
       }
      }
      
    })
  }

  render () {
  
    return (
      <div className="App">
        <header className="App-header">
          {
            
            this.state.ratingsList.map(rating =>
              
              <Rating 
                key={rating.id}  
                id={rating.id}
                onClick={this.handleClick} 
                selected={rating.selected}
                currentHighest={this.state.currentRating}/>
              
              )
          }

        </header>
      </div>
    )
  }
}

export default App;
