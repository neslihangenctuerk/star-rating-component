import React from 'react'

class Rating extends React.Component {

    handleClick = (event) => {
        this.props.onClick(event.target.value)
    }

    render () {
        return (
            <div>

                <button 
                    value={this.props.id} 
                    className={this.props.selected ? 'yellow' : 'pink'} 
                    onClick={this.handleClick}>

                    { this.props.color }

                </button>
            </div>
        )
    }
}   
    


export default Rating