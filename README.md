This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## General
This component is inspired by the 5-star review section on product sites.
Main purpose is the reusability throughout different projects.

Currently the state and functions that update the state are in the __App.js__.
Each "Star", which right now is symbolized by a regular square, is pink.

When the user selects a box, as exptected, every box including the selected one turns yellow to indicate the rating.

Changing the rating to a lower or higher one is also possible and will be reflected in the state.





__next possible TODO__: 
+ configure styling w/ reusability in mind


## Installation and Usage

### `npm install`


### `npm start`
This runs the app in development mode.
Visit [http://localhost:3000](http://localhost:3000) in the browser.



## Tools
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). Which is licensed under: [LICENSE](https://github.com/facebook/create-react-app/blob/master/LICENSE). 
Also there is a copy of this license under __NOTICE.md__ in this project folder.

